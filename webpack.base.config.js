const path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var DEV_SERVER_PORT = parseInt(process.env.PORT, 10) || 3000;


module.exports = {
    context: path.join(__dirname, '/src'),
    entry: {
        app: './index.jsx',
        vendor: ['react', 'react-dom',"react-router-dom", 'react-redux', 'redux']
    },
    resolve: {
        extensions: ['.js', '.jsx', '.scss', '.css']
    },
    output: {
        path: path.join(__dirname, '/dist/'),
        filename: '[name].bundle.js',
        sourceMapFilename: '[name].map',
        publicPath: '/dist/'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,         // Match both .js and .jsx files
                exclude: /node_modules(?!\/webpack-dev-server)/,
                loader: "babel-loader",
                query: {
                    presets: ["babel-preset-es2015", "babel-preset-es2016", "babel-preset-es2017", 'react', "stage-2"]
                }
            },
            {
                test: /\.css$/,
                // exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            query: {
                                modules: true,
                                localIdentName: '[local]'
                            }
                        }
                    ]
                }),
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            query: {
                                modules: true,
                                sourceMap: true,
                                importLoaders: 2,
                                localIdentName: '[name]__[local]'
                            }
                        },
                        'sass-loader'
                    ]
                }),
            },
            {
                test: /\.(jpg|png|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]',
                },
            }
        ]
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: 'vendor.bundle.js'
        }),
        new HtmlWebpackPlugin({
            title: 'Талоны',
            template: path.join(__dirname, '/src/index.ejs'),
            filename: path.join(__dirname, '/index.html'),
            alwaysWriteToDisk: true
        }),
        new ExtractTextPlugin('styles.out.css')
    ],
    devServer: {
        host: '0.0.0.0',
        compress: true,
        port: DEV_SERVER_PORT,
        historyApiFallback: true,
        disableHostCheck: true,
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        proxy: {
            '/api': {
                target: 'https://talons-famcs.herokuapp.com',
                pathRewrite: {'^/api' : '/api'},
                changeOrigin: true,
                secure: false
            }
        }
    }
}
