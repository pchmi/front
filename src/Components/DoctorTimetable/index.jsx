import React, { Component } from 'react'
import styles from './scss/index.scss';

export class DoctorTimetable extends Component {
  render() {
    let { doctors, headers, renderRow, onSubmit } = this.props;
    return (
      <div className={styles.doctors_table}>
        <div className={styles.table}>
          {renderHeader(headers)}
          {renderDoctorsTableBody(doctors, renderRow, onSubmit)}
        </div>
      </div>
    )
  }
}

const renderDoctorsTableBody = (doctors, renderRow, onSubmit) => (
  doctors.map((doc, i) => {
    return <form key={i} onSubmit={e => onSubmit(e, doc)}>
      {renderRow(doc)}
    </form>
  })
)

const renderHeader = (headers) => (
  <div className={styles.head}>
    {headers.map((header, i) => (
      <div key={i}>{header}</div>
    ))}
  </div>
)