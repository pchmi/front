import React, { Component } from 'react'
import styles from './scss/index.scss';
import commonStyles from '../../scss/common.scss';

export class Footer extends Component {
  render() {
    let { title, footerLeftTitle } = this.props;
    return (<footer>
      <hr className={commonStyles.horizontal_line} />
      <div className={styles.footer}>
        <div className={styles.left_section}>
          <h2>Мы в соцсетях: </h2>
          <div className={styles.social_network_logo} />
        </div>
        <div className={styles.right_section}>
          <h2 className={commonStyles.text_right}>{title}</h2>
          <p>Copyright (c) 2017. All rights reserved</p>
        </div>
      </div>
    </footer>
    )
  }
}
