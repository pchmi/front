import React, { Component } from 'react'
import styles from './scss/index.scss';

export class GridLayout extends Component {
  render() {
    return (
      <div className={styles.grid}>
        {this.props.children}
      </div>
    )
  }
}

export class GridItem extends Component {
    render() {
      return (
        <div className={styles.grid_item}>
          {this.props.children}
        </div>
      )
    }
  }