import React, { Component } from 'react'
import styles from './scss/index.scss';
import { renderOptions, possibleAreas } from '../../common/formValues';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import cssStyles from 'react-day-picker/lib/style.css';
import { connectCustom } from '../../common/reduxTools';
import { getQueryString } from '../../common/restData';
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment';
import { loadSpecialists } from '../../common/specReducer';

class Filter extends Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.formSubmit = this.formSubmit.bind(this);
    this.state = {
      doctorName: getQueryString('doc'),
      doctorType: getQueryString('docType'),
      areaNumber: '',
      date: getQueryString('date')
    }
  }

  componentWillMount() {
    this.props.loadSpecialities();
  }

  componentDidMount() {
    if (this.props.onLoadCallBack) {
      this.props.onLoadCallBack(this.state);
    }
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  formSubmit = (state, callback) => {
    let filter = this;
    return e => {
      e.preventDefault();
      if (filter.dateInput) {
        state.date = filter.dateInput.getInput().value;
      }
      callback(e, state);
    }
  }

  render() {
    let { doctorName, doctorType, areaNumber, date } = this.state;
    let { specialities = [], possibleAreas, btnName, onFormSubmit } = this.props;
    let { showDatePicker = true, showDepartment = false } = this.props;
    let docTypeFromUrl = getQueryString('docType');
    let specFromUrl = specialities.filter(spec => spec.id == docTypeFromUrl)[0];
    return (
      <div className={styles.filter_panel}>
        <form className={styles.filter_form} onSubmit={this.formSubmit(this.state, onFormSubmit)}>
          <div>
            <input name="doctorName" value={doctorName} onChange={this.handleInputChange} placeholder="Врач" />
          </div>
          <div>
            <select value={doctorType} onChange={this.handleInputChange} name="doctorType" ref={el => (this.doctorTypeInput = el)}>
              <option value="">Все</option>
              {renderOptions(specialities, "id", "speciality_name")}
            </select>
          </div>
          {showDepartment && <div>
            <select value={areaNumber} onChange={this.handleInputChange} name="areaNumber">
              {renderOptions(possibleAreas)}
            </select>
          </div>}
          {showDatePicker &&
            <DayPickerInput value={date} format='DD-MM-YYYY' formatDate={formatDate} parseDate={parseDate} ref={el => (this.dateInput = el)} />}
          <br />
          <button type="submit" className={styles.submit}>{btnName}</button>
        </form>
        <div className={styles.filter_res}>
          {docTypeFromUrl && specFromUrl && 
            <div>Результаты по специальности: {specFromUrl.speciality_name}</div>}
            {getQueryString('doc') && <div>Врач: {getQueryString('doc')}</div>}
        </div>
      </div>
    )
  }
}

const mapDispatchToProsps = (dispatch) => {
  return {
    loadSpecialities: () => dispatch(loadSpecialists())
  }
}

export const FilterComponent = connectCustom('specialities', mapDispatchToProsps)(Filter);