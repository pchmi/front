import React, { Component } from 'react'

export class Input extends Component {
  render() {
    let { name, onValidate } = this.props;
    return (
      <input name={name} />
    )
  }
}

export const renderInput = (name) => (
  <input name={name}/>
)