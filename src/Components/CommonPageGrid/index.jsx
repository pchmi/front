import React, { Component } from 'react'
import styles from './scss/index.scss';
import {links} from '../../common/links';
import { Link } from 'react-router-dom';

export class CommonPageGrid extends Component {
    render() {
        let { activeIndex } = this.props;
        return (
            <div className={styles.talons_page_grid}>
                <LeftTalonPagen menus={links} activeIndex={activeIndex} />
                {this.props.children}
            </div>
        )
    }
}

class LeftTalonPagen extends Component {
    render() {
        let { menus, activeIndex } = this.props;
        return (
            <div className={styles.left_menu_panel}>
                {menus.map((menu, index) => (
                    <div key={index} className={activeIndex == index ? styles.active : ''}>
                        <Link to={menu.href}>{menu.title}</Link>
                    </div>
                ))}
            </div>
        )
    }
}