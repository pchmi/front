import React, { Component } from 'react'
import styles from './scss/index.scss';

export class Search extends Component {
    render() {
        return (
            <div className={styles.search_component}>
                <input type="search" />
            </div>
        )
    }
}
