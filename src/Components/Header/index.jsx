import React, { Component } from 'react'
import styles from './scss/index.scss';
import { Search } from '../Search/index';
import logoImage from './img/logo.png';
import { Link } from 'react-router-dom';
import commonStyles from '../../scss/common.scss';
import { connectCustom } from '../../common/reduxTools';
import { extractUserInfo, logoutUserAction } from '../../common/userReducer';
import {LoadingComponent} from '../LoadingComponent/index';

class HeaderComponent extends Component {
    componentWillMount() {
        let { loadUserInfo } = this.props;
        loadUserInfo();
    }

    render() {
        let { title, currentUser = {}, logout, logoutInProgress } = this.props;
        let authorized = currentUser.email;
        return (
            <div>
                <div className={styles.header}>
                    <div className={styles.header_left_section}>
                        <Link to="/">
                            <img src={logoImage} className={styles.header_image} />
                        </Link>
                        <Link to="/">
                            <h2 className={styles.header_logo}>{title}</h2>
                        </Link>
                    </div>
                    <div className={`${styles.header_section_login} ${commonStyles.text_right}`}>
                        {authorized && <AuthorizedHeader currentUser={currentUser} logout={logout} />}
                        {!authorized && <UnathorizedHeader />}
                        {/* <Search /> */}
                    </div>
                </div>
                <hr className={styles.horizontal_line} />
                {logoutInProgress && <LoadingComponent/>}
            </div>
        )
    }
}

class UnathorizedHeader extends Component {
    render() {
        return (
            <div>
                <Link to="/login">
                    Войти в систему</Link>
                &nbsp;| <Link to="/register">Регистрация</Link>
            </div>
        )
    }
}

class AuthorizedHeader extends Component {
    wrapLogout = (logout) => {
        return e => {
            e.preventDefault();
            logout();
        }
    }

    render() {
        let { currentUser, logout } = this.props;
        return (
            <div>
                <div>Здравствуйте, {currentUser.name}</div>
                <Link to="/my-talons">
                    Заказанные талоны</Link>
                &nbsp;| <a href="#" onClick={this.wrapLogout(logout)}>Выйти</a>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadUserInfo: _ => dispatch(extractUserInfo()),
        logout: _ => dispatch(logoutUserAction())
    }
}

export const Header = connectCustom('user', mapDispatchToProps)(HeaderComponent);