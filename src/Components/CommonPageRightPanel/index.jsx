import React, { Component } from 'react'
import commonStyles from '../../scss/common.scss';
import styles from './scss/index.scss';
import { Link } from 'react-router-dom';

export class CommonPageRightPanel extends Component {
  render() {
    let { title } = this.props;
    return (
      <div>
        <div>
          <h1 className={commonStyles.text_center}>{title}</h1>
          <Breadcrumbs breadcrumbs={breadcrumbBuilder(title)} />
        </div>
        {this.props.children}
      </div>
    )
  }
}

const breadcrumbBuilder = (name) => {
  return [{
    href: '/', name: 'Домой'
  },
  {
    href: location.pathname + location.search, name: name
  }]
}

class Breadcrumbs extends Component {
  render() {
    let { breadcrumbs } = this.props;
    return <div className={styles.breadcrumbs}>
      {breadcrumbs.map((item, i) => (
        <div className={styles.breadcrumb_item} key={i}>
          <Link to={item.href}>
            {item.name}
          </Link>
        </div>
      ))
      }
    </div>
  }
}