import React, { Component } from 'react'
import styles from './scss/loader-styles.scss';

export class LoadingComponent extends Component {
  render() {
    return (
      <div className={styles.loader_wrapper}>
        <div className={styles.loader} />
      </div>
    )
  }
}

export class LocalLoadingComponent extends Component {
  render() {
    return (
      <div className={styles.relative_container}>
        <div className={`${styles.loader_wrapper} ${styles.local} ${styles.no_back}`}>
          <div className={`${styles.loader} ${styles.dark}`} />
        </div>
      </div>
    )
  }
}