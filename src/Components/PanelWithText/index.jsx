import React, { Component } from 'react'
import styles from './scss/index.scss';
import commonStyles from '../../scss/common.scss';

export class PanelWithText extends Component {
    render() {
        let { title, img, text } = this.props;
        return (
            <div>
                <h2>{title}</h2>
                <div className={styles.img_section}>
                    <div>
                        <img src={img} />
                    </div>
                    <p className={commonStyles.text_center}>{text}</p>
                </div>
                <hr className={commonStyles.horizontal_line} />
            </div>
        )
    }
}
