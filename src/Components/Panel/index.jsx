import React, { Component } from 'react'
import styles from './scss/index.scss';
import { Link } from 'react-router-dom';

export class Panel extends Component {
    render() {
        let { title, img, href } = this.props;
        return (
            <div>
                <Link to={href}>
                    <div className={styles.panel}>
                        <img src={img} />
                        <p>{title}</p>
                    </div>
                </Link>
            </div>
        )
    }
}
