import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, combineReducers } from 'redux'
import { BrowserRouter, Route, Router, Switch } from "react-router-dom";
import { BaseScene } from './Scenes/BaseScene/index';
import { Home } from './Scenes/Home/index';
import { Timetable } from './Scenes/Timetable/index';
import { actionConverterMiddleware } from './common/apiMiddleware';
import { timeTableReducer , talonsReducer} from './common/doctorsReducers';
import { Talons } from './Scenes/Talons/index';
import {Login} from './Scenes/Login/index';
import {loginReducer} from './common/loginReducer';
import {Register} from './Scenes/Register/index';
import { userReducer } from './common/userReducer';
import {MyTalons} from './Scenes/MyTalons/index';
import {accountReducer} from './common/accountReducer';
import {specReducer} from './common/specReducer';

let initialState = {
    timetable: {},
    talons : {},
    login : {},
    user: {},
    account: {},
    specialities : {}
};
const tempReducer = (state = {}, action) => (state);

let store = createStore(combineReducers({
    timetable: timeTableReducer,
    talons: talonsReducer,
    login: loginReducer,
    user: userReducer,
    account: accountReducer,
    specialities: specReducer
}), initialState, applyMiddleware(actionConverterMiddleware, thunk));

render(
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/timetable" component={Timetable} />
                <Route path="/talons" component={Talons} />
                <Route path="/login" component={Login} />
                <Route path="/register" component={Register} />
                <Route path="/my-talons" component={MyTalons} />
                <Route component={Home} />
            </Switch>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
