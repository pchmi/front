import React from 'react';

export const possibleDoctorTypes = [
  {name: 'Учатковый терапевт',value: 1},
  {name: 'Нарколог',value: 2},
  {name: 'Окулист',value: 3}
];

export const possibleAreas = [
  {name: 'Участок №1',value: 1},
  {name: 'Участок №2',value: 2},
  {name: 'Участок №3',value: 3},
  {name: 'Участок №4',value: 4}
];

export const renderOptions = (options, value = "value", name = "name") => {
  return options.map((type, i) => (
    <option value={type[value]} key={i}>{type[name]}</option>
  ));
}