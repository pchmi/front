import { getWeekTimetable, getTalons, submitTalon } from './restData';
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment';

// import { addApiMapping } from "./apiMiddleware";

/**
 * here time table for doctors section
 */
// #region Week time table
const LOAD_DOCTORS_TIME_TABLE = "LOAD_DOCTORS_TIME_TABLE";
const TIME_TABLE_REQUEST_START = 'TIME_TABLE_REQUEST_START';
const TIME_TABLE_REQUEST_END = 'TIME_TABLE_REQUEST_END';

const loadedDoctorsWeekTimetable = (data) => ({
  type: LOAD_DOCTORS_TIME_TABLE,
  data
})

export const loadDoctorsTimeTableAction = (filters = {}) => {
  return dispatch => {
    dispatch(loadedDoctorsWeekTimetable([]))
    dispatch({ type: TIME_TABLE_REQUEST_START })
    getWeekTimetable({specialityId : filters.doctorType})
      .then(data => {
        let timetable = data;
        if (filters.doctorName) {
          timetable = timetable.filter(rec => rec.doctor_name.toLowerCase().indexOf(filters.doctorName.toLowerCase()) != -1)
        }
        dispatch(loadedDoctorsWeekTimetable(timetable))
        dispatch({ type: TIME_TABLE_REQUEST_END })
      })
  }
}

export const timeTableReducer = (store = {}, action) => {
  switch (action.type) {
    case LOAD_DOCTORS_TIME_TABLE:
      return {
        ...store,
        doctorsTimeTable: action.data
      }
    case TIME_TABLE_REQUEST_START:
      return {
        ...store,
        requestInProgress: true
      }
    case TIME_TABLE_REQUEST_END:
      return {
        ...store,
        requestInProgress: false
      }
    default:
      return store;
  }
}

//#endregion

/**
 * here doctor talons section
 */

//#region Talons
const LOAD_DOCTORS_TALONS = "LOAD_DOCTORS_TALONS";
const TALONS_REQUEST_START = "TALONS_REQUEST_START";
const APPROVE_TALONS_REQUEST_START = "APPROVE_TALONS_REQUEST_START";
const TALONS_REQUEST_END = 'TALONS_REQUEST_END';
const APPROVE_TALONS_REQUEST_END = 'APPROVE_TALONS_REQUEST_END';
const TALON_APPROVE_DONE = "TALON_APPROVE_DONE";

const loadedDoctorsTalons = (data) => ({
  type: LOAD_DOCTORS_TALONS,
  data
})

let doctorTalons = [];
let allTalons = [];

export const submitTalonAction = (id) => {
  return dispatch => {
    dispatch({ type: APPROVE_TALONS_REQUEST_START })
    submitTalon(id)
      .then(data => {
        dispatch({ type: TALON_APPROVE_DONE, data })
        dispatch({ type: APPROVE_TALONS_REQUEST_END })
      })
  }
}

export const loadDoctorsTalonsAction = (filters = {}, force = false) => {
  if (!force && allTalons.length) {
    let currentTalons = applyFilters(allTalons, filters);
    return dispatch => dispatch(loadedDoctorsTalons(finalPostProcess(currentTalons)));
  }
  return dispatch => {
    dispatch({ type: TALONS_REQUEST_START });
    getTalons()
      .then(data => {
        allTalons = processTalons(data.filter(el => (
          el.patient_id == null
        )).sort((tal_1, tal_2) => (
          new Date(tal_1.time).getTime() - new Date(tal_2.time).getTime()
        )));
        let currentTalons = applyFilters(allTalons, filters);
        doctorTalons = finalPostProcess(currentTalons);
        dispatch(loadedDoctorsTalons(doctorTalons, filters));
        dispatch({ type: TALONS_REQUEST_END });
      })
  }
}

const defaultTimeOffset = (new Date()).getTimezoneOffset() * 60000;

export const talonDate = (talon) => (
  new Date(new Date(talon.time).valueOf() + defaultTimeOffset)
)

export const talonDateFullWithWeek = (talon) => {
  let date = talonDate(talon);
  return `${dayWeeks[date.getDay() - 1]} - ${formatDate(date, 'DD.MM HH-mm')}`;
}


const dayWeeks = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'];

const processTalons = (doctorTalons) => {
  let frmDate = formatDate;
  let prsDate = parseDate;
  return doctorTalons.map(talon => ({
    talonTime: talonDate(talon),
    talonTimeStr: formatDate(talonDate(talon), 'DD.MM HH-mm'),
    talonAtHome: talon.departure ? ' (Выезд на дом)' : '',
    talonDayWeek: dayWeeks[talonDate(talon).getDay() - 1],
    doc_id: talon.doctor_id,
    isAtHome: talon.departure,
    id: talon.id,
    docName: talon.doctor_name,
    speciality: talon.doctor_speciality,
    doctor_speciality_id  : talon.doctor_speciality_id
  }));
}

const finalPostProcess = (talons) => {
  let result = [];
  for (let i = 0; i < talons.length; i++) {
    let talon = talons[i];
    let item = result[talon.doc_id];
    if (!item) {
      item = {
        docName: talon.docName,
        speciality: talon.speciality,
        doc_id: talon.doc_id,
        talons: []
      }
      result[talon.doc_id] = item;
    }
    item.talons.push({
      time: talon.talonTime,
      timeStr: `${talon.talonDayWeek} - ${talon.talonTimeStr} ${talon.talonAtHome}`,
      id: talon.id
    })
  }
  return result.filter(r => (r));
}

const docNameFilter = (name) => {
  if (!name) {
    return talon => true;
  }
  return (talon) => (talon.docName.toLowerCase().indexOf(name.toLowerCase()) != -1)
}

const dateFilter = (date) => {
  if (!date) {
    return talon => true;
  }
  return (talon) => (formatDate(talon.talonTime, 'DD-MM-YYYY') == date)
}

const specFilter = (spec) => {
  if ( !spec){
    return talon => true;
  }
  return (talon) => {
    return talon.doctor_speciality_id == spec
  }
}

const applyFilters = (talons, filters) => {
  return talons.filter(docNameFilter(filters.doctorName))
    .filter(dateFilter(filters.date))
    .filter(specFilter(filters.doctorType));
}

export const talonsReducer = (store = {}, action) => {
  switch (action.type) {
    case LOAD_DOCTORS_TALONS:
      return {
        ...store,
        doctorsTalons: action.data
      }
    case TALONS_REQUEST_START:
      console.log('request start')
      return {
        ...store,
        requestInProgress: true
      }
    case TALONS_REQUEST_END:
      console.log('request ended')
      return {
        ...store,
        requestInProgress: false
      }
    case TALON_APPROVE_DONE:
      return {
        ...store,
        approvedTalon: action.data
      }
    case APPROVE_TALONS_REQUEST_START:
      return {
        ...store,
        approveRequestInProgress: true
      }
    case APPROVE_TALONS_REQUEST_END:
      return {
        ...store,
        approveRequestInProgress: false
      }
    default:
      return store;
  }
}

//#endregion