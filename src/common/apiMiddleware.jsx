const apiMapping = {};

export const addApiMapping = (actionType, actionConverter) => {
  apiMapping[actionType] = actionConverter;
}

export const actionConverterMiddleware = store => next => action => {
  let type = action.type;
  let actionConverter = apiMapping[type];
  if (actionConverter) {
    let newAction = actionConverter(action);
    if (newAction.type !== action.type) {
      store.dispatch(newAction);
      return;
    }
  }
  return next(action)
}