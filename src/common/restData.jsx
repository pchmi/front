import { UserConstants } from './constants';
const urls = {
  createUser: '/api/people',
  loginUser: '/api/login',
  currentUser: '/api/user/current_user',
  logOut: '/api/logout',
  timeTableUrl: '/api/week_timetable',
  timetable: '/api/timetable',
  approveTalonUrl: '/api/timetable/update/',
  myTalonsUrl: '/api/user/my_talons',
  specialities: '/api/specialities',
  revertTalon: '/api/timetable/revert/'
}

const headers = new Headers({
  "Content-Type": "application/json"
})

//#region Users

export const createUserCall = (userName, firstAndLastName, password, birthday) => {
  return post(urls.createUser, {
    email: userName,
    password: password,
    role_id: UserConstants.patientRole,
    name: firstAndLastName,
    birthday
  })
}

export const loginUserCall = (userName, password) => {
  return post(urls.loginUser, {
    email: userName,
    password: password
  })
}

export const exportUserInfo = () => {
  return callGet(urls.currentUser)
}

export const logoutUser = () => {
  return callGet(urls.logOut)
}

export const myTalons = () => {
  return callGet(urls.myTalonsUrl);
}

export const revertTalon = (id) => {
  return put(urls.revertTalon + id, {})
}

//#endregion 


//#region Week timetable API

export const getWeekTimetable = (filters = {}) => {
  let specialityId = filters.specialityId;
  let filter = '';
  if (specialityId) {
    filter = `/${specialityId}`;
  }
  return callGet(urls.timeTableUrl + filter)
}

//#endregion

//#region Talons API

export const getTalons = () => {
  return callGet(urls.timetable)
}

export const submitTalon = (id) => {
  return put(urls.approveTalonUrl + id, {})
}

//#endregion


//#region Specialists

export const callSpecialities = () => {
  return callGet(urls.specialities);
}

//#endregion

//#region common methods

const callGet = (url, data) => {
  return fetch(url, {
    credentials: "same-origin"
  })
    .then(response => {
      return response.json();
    })
}

const post = (url, data) => {
  return fetch(url, {
    method: 'POST',
    body: JSON.stringify(data),
    headers,
    credentials: "same-origin"
  })
    .then(response => {
      return response.json();
    })
}

const put = (url, data) => {
  return fetch(url, {
    method: 'PUT',
    body: JSON.stringify(data),
    headers,
    credentials: "same-origin"
  }).then(response => {
      return response.json();
    })
}

//#endregion

export const getQueryString = (field, defValue = "", url = "") => {
  var href = url ? url : decodeURI(window.location.href);
  var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
  var string = reg.exec(href);
  return string ? string[1] : defValue;
};

