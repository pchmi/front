import {callSpecialities} from './restData';

const SPEC_LOADED = "SPEC_LOADED";

export const loadSpecialists = () => {
  return dispatch => {
    callSpecialities()
    .then(data => dispatch({type: SPEC_LOADED, data}))
  }
}


export const specReducer = (store = {}, action) => {
  switch (action.type) {
    case SPEC_LOADED:
      return {
        ...store,
        specialities: action.data
      }
    default:
      return store;
  }
}

