import { myTalons, revertTalon } from './restData';

const MY_TALONS_REQUEST = "MY_TALONS_REQUEST";
const MY_TALONS_LOADED = "MY_TALONS_LOADED";
const MY_TALONS_REQ_END = "MY_TALONS_REQ_END";


export const revertTalonAction = (id) => {
  return dispatch => {
    dispatch({ type: MY_TALONS_REQUEST })
    revertTalon(id)
      .then(_ => {
        getMyTalonsAction()(dispatch)
      })
  }
}

export const getMyTalonsAction = () => {
  return dispatch => {
    dispatch({ type: MY_TALONS_REQUEST })
    dispatch({ type: MY_TALONS_LOADED, data: [] })
    myTalons()
      .then(data => dispatch({ type: MY_TALONS_LOADED, data }))
      .then(_ => dispatch({ type: MY_TALONS_REQ_END }))
      .catch(err => {
        location.pathname = "/"
      })
  }
}



export const accountReducer = (store = {}, action) => {
  switch (action.type) {
    case MY_TALONS_REQUEST:
      return {
        ...store,
        requestInProgress: true
      }
    case MY_TALONS_REQ_END:
      return {
        ...store,
        requestInProgress: false
      }
    case MY_TALONS_LOADED:
      return {
        ...store,
        talons: action.data
      }
    default:
      return store;
  }
}