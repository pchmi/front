import { createUserCall, loginUserCall, exportUserInfo, logoutUser } from './restData';
const CREATE_USER_REQUEST = "CREATE_USER_REQUEST";
const CREATE_USER_SUCCESS = "CREATE_USER_SUCCESS";
const CREATE_USER_FAILURE = "CREATE_USER_FAILURE";
const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
const USER_INFO_RETRIEVE_SUCCESS = "USER_INFO_RETRIEVE_SUCCESS";
const LOGOUT_IN_PROGRESS = "LOGOUT_IN_PROGRESS";
const LOGOUT_FINISHED = "LOGOUT_FINISHED";

const LOGIN_IN_PROGRESS = "LOGIN_IN_PROGRESS";
const LOGIN_PROGRESS_END = "LOGIN_PROGRESS_END";
const USER_ALREADY_EXISTS = "USER_ALREADY_EXISTS";

const createUserSucceed = (data) => ({
  type: CREATE_USER_SUCCESS,
  data
})

const loginUserSucceed = (data) => ({
  type: LOGIN_USER_SUCCESS,
  data
})

const userInfoLoaded = (data) => ({
  type: USER_INFO_RETRIEVE_SUCCESS,
  data
})

export const extractUserInfo = () => {
  return dispatch => {
    exportUserInfo()
      .then(data => dispatch(userInfoLoaded(data)))
      .catch(error => {
        window.userLogged = false;
        console.warn(error);
      })
  }
}

export const logoutUserAction = () => {
  return dispatch => {
    dispatch({ type: LOGOUT_IN_PROGRESS })
    logoutUser()
      .then(_ => exportUserInfo())
      .then(data => dispatch(userInfoLoaded(data)))
      .then(data => dispatch({ type: LOGOUT_FINISHED }))
      .then(_ => location.pathname = "/")
      .catch(error => {
        throw (error);
      })
  }
}

export const createUserAction = (userName, firstAndLastName, password, birthday) => {
  return dispatch => {
    dispatch({ type: LOGIN_IN_PROGRESS });
    createUserCall(userName, firstAndLastName, password, birthday)
      .then(data => {
        if (!data.Error) {
          return dispatch(createUserSucceed(data))
        } else {
          return dispatch({ type: USER_ALREADY_EXISTS })
        }
      })
      .then(data => {
        if (data.type == CREATE_USER_SUCCESS) {
          return loginUserCall(userName, password)
        } else if (data.type == USER_ALREADY_EXISTS) {
          return;
        }
      })
      .then((data = {}) => {
        if (data.email) {
          location.pathname = "/";
        } else {
          dispatch({ type: LOGIN_PROGRESS_END });
        }
      })
      .catch(error => {
        throw (error);
      })
  }
}

export const loginUserAction = (userName, password) => {
  return dispatch => {
    dispatch({ type: LOGIN_IN_PROGRESS });
    loginUserCall(userName, password)
      .then(data => {
        dispatch(loginUserSucceed(data))
        dispatch({ type: LOGIN_PROGRESS_END });
        dispatch(userInfoLoaded(data))
      })
      .catch(error => {
        throw (error);
      })
  }
}

export const userReducer = (store = {}, action) => {
  switch (action.type) {
    case CREATE_USER_SUCCESS:
      return {
        ...store,
        data: action.data
      }
    case LOGIN_USER_SUCCESS:
      return {
        ...store,
        data: action.data
      }
    case USER_INFO_RETRIEVE_SUCCESS:
      window.userLogged = !action.data.Error;
      return {
        ...store,
        currentUser: action.data
      }
    case LOGOUT_IN_PROGRESS:
      return {
        ...store,
        logoutInProgress: true
      }
    case LOGOUT_FINISHED:
      return {
        ...store,
        logoutInProgress: false
      }
    case LOGIN_IN_PROGRESS:
      return {
        ...store,
        loginInProgress: true
      }
    case LOGIN_PROGRESS_END:
      return {
        ...store,
        loginInProgress: false
      }
    case USER_ALREADY_EXISTS:
      return {
        ...store,
        userAlreadyExists: true
      }
    default:
      return store;
  }
}