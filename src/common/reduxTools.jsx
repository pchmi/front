import { connect } from 'react-redux'

const defaultMapStateToProps = (statePart) => {
  return (state) => {
    let props = state[statePart];
    return props ? props : {};
  }
}

export const connectCustom = (statePart, mapDispatchToProps) => {
  return connect(defaultMapStateToProps(statePart), mapDispatchToProps)
}