import React, { Component } from 'react'
import { CommonPageGrid } from '../../Components/CommonPageGrid/index';
import { BaseScene } from '../BaseScene/index';
import { CommonPageRightPanel } from '../../Components/CommonPageRightPanel/index';
import { FilterComponent } from '../../Components/FilterComponent/index';
import { possibleDoctorTypes, possibleAreas } from '../../common/formValues';
import { DoctorTimetable } from '../../Components/DoctorTimetable/index';
import { connectCustom } from '../../common/reduxTools';
import { loadDoctorsTimeTableAction } from '../../common/doctorsReducers';
import { Link } from 'react-router-dom';
import commonStyles from '../../scss/common.scss';
import {LocalLoadingComponent, LoadingComponent} from '../../Components/LoadingComponent/index';

const day_keys = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']

class TimetableClear extends Component {
  constructor(props){
    super(props);
    this.onFilter = this.onFilter.bind(this);
  }
  componentWillMount() {
    // this.props.loadTimetable();
  }

  loadTalonsByState(state, props) {
    let { history } = props;
    let oldQuery = decodeURI(history.location.search);
    let newQuery = "";
    if (state.doctorName) {
      newQuery += `doc=${state.doctorName}&`;
    }
    if (state.doctorType) {
      newQuery += `docType=${state.doctorType}&`;
    }
    if (state.date) {
      newQuery += `date=${state.date}&`;
    }
    newQuery = newQuery.substr(0, newQuery.length - 1);
    if (newQuery != oldQuery) {
      history.push(history.location.pathname + '?' + newQuery);
    }
    props.loadTimetable(state);
  }

  onFilter(e, state) {
    this.loadTalonsByState(state, this.props);
  }

  render() {
    let { doctorsTimeTable = [], requestInProgress } = this.props;
    return (<BaseScene>
      <CommonPageGrid activeIndex={0}>
        <CommonPageRightPanel title="Расписание работы врачей">
          <FilterComponent possibleDoctorTypes={possibleDoctorTypes} possibleAreas={possibleAreas} btnName="Найти"
          onFormSubmit={this.onFilter} onLoadCallBack={state => this.loadTalonsByState(state, this.props)} showDatePicker={false}/>
          <DoctorTimetable doctors={doctorsTimeTable} headers={['Врач', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'/*, 'Действие'*/]} renderRow={renderRow} />
          {requestInProgress && <LocalLoadingComponent/>}
        </CommonPageRightPanel>
      </CommonPageGrid>
    </BaseScene>
    )
  }
}

const renderRow = (doc) => {
  return [
    <div key="name">
      <p className={commonStyles.bold_par}>{doc.doctor_name}</p>
      {doc.speciality_name}</div>,
    day_keys.map(item => (
      <div key={item}>
        {doc[item]}
      </div>
    )),
    // <div key="submit">
    //   <Link to='/doctor-at-home'>Записаться на дом</Link>
    // </div>
  ];
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadTimetable: filters => dispatch(loadDoctorsTimeTableAction(filters))
  }
}

export const Timetable = connectCustom('timetable', mapDispatchToProps)(TimetableClear); 