import React, { Component } from 'react'
import { connectCustom } from '../../common/reduxTools';
import { BaseScene } from '../BaseScene/index';
import { getMyTalonsAction, revertTalonAction } from '../../common/accountReducer';
import { DoctorTimetable } from '../../Components/DoctorTimetable/index';
import commonStyles from '../../scss/common.scss';
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment';
import { talonDateFullWithWeek } from '../../common/doctorsReducers';
import { LocalLoadingComponent } from '../../Components/LoadingComponent/index';
import { Link } from 'react-router-dom';

class MyTalonsComponent extends Component {
  componentWillMount() {
    this.props.loadTalons();
  }

  render() {
    let { talons = [], requestInProgress, revertTalon } = this.props;
    return (
      <BaseScene>
        <TalonsSection talons={talons} requestInProgress={requestInProgress} revertTalon={revertTalon} />
      </BaseScene>
    )
  }
}

class ChildrenSection extends Component{
  render(){
    return (
      <div>ChildrenSection</div>
    )
  }
}

class TalonsSection extends Component {
  render() {
    let { talons = [], requestInProgress, revertTalon } = this.props;
    return (
      <div>
        <h1 className={commonStyles.text_center}>Ваши заказанные талоны</h1>
        <DoctorTimetable doctors={talons} headers={['Врач', 'Дата приема', 'Примечания', 'Действие']}
          renderRow={renderRow(revertTalon)} onSubmit={e => { e.preventDefault() }} />
        {requestInProgress && <LocalLoadingComponent />}
        {!requestInProgress && talons.length == 0 && <div className={commonStyles.no_results_found}>
          Талонов не найдено<br />
          <Link to="/talons">Перейти к талонам</Link>
        </div>}
      </div>
    )
  }
}

const renderRow = (talonRevert) => (talon) => {
  return [
    <div key="docName">
      <p className={commonStyles.bold_par}>{talon.doctor_name}</p>
      {talon.doctor_speciality}</div>,
    <div key="time">{talonDateFullWithWeek(talon)}</div>,
    <div key="notes">
      {talon.departure && <p>Вызов на дом</p>}
    </div>,
    <div key="revert">
      {new Date(talon.time) > new Date() && <button onClick={_ => talonRevert(talon.id)}>Отменить талон</button>}
    </div>
  ];
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadTalons: _ => dispatch(getMyTalonsAction()),
    revertTalon: id => dispatch(revertTalonAction(id))
  }
}

export const MyTalons = connectCustom('account', mapDispatchToProps)(MyTalonsComponent);