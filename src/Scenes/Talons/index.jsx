import React, { Component } from 'react'
import { connectCustom } from '../../common/reduxTools';
import { BaseScene } from '../BaseScene/index';
import { CommonPageGrid } from '../../Components/CommonPageGrid/index';
import { CommonPageRightPanel } from '../../Components/CommonPageRightPanel/index';
import { FilterComponent } from '../../Components/FilterComponent/index';
import { DoctorTimetable } from '../../Components/DoctorTimetable/index';
import { loadDoctorsTimeTableAction, loadDoctorsTalonsAction , submitTalonAction} from '../../common/doctorsReducers';
import { possibleDoctorTypes, possibleAreas } from '../../common/formValues';
import { Modal, ModalHeader, ModalContent, ModalFooter } from '../../Components/Modal';
import styles from './scss/index.scss';
import commonStyles from '../../scss/common.scss';
import { LoadingComponent, LocalLoadingComponent } from '../../Components/LoadingComponent/index';
import { Link } from 'react-router-dom';

class TalonsClear extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalOpen: false
    }
    this.onTalonClickSubmit = this.onTalonClickSubmit.bind(this);
    this.onModalClose = this.onModalClose.bind(this);
    this.onTalonApprove = this.onTalonApprove.bind(this);
    this.onFilter = this.onFilter.bind(this);
  }

  componentWillMount() {
    // this.props.loadDoctorTalons(this.state);
  }

  onTalonClickSubmit(e, doc) {
    e.preventDefault();
    let selectedTalon = doc.talons.filter(tal => tal.id == document.getElementById("talon-" + doc.doc_id).value)[0];
    this.setState({
      modalOpen: true,
      talon: {
        docName: doc.docName,
        docSpec: doc.speciality,
        talonDate: selectedTalon.timeStr,
        talonId: selectedTalon.id
      }
    })
  }

  onModalClose(e) {
    this.setState({ modalOpen: false })
  }

  onTalonApprove() {
    console.log('approving talon: ', this.state.talon)
    this.setState({ modalOpen: false })
    this.props.submitTalon(this.state.talon.talonId);
  }

  loadTalonsByState(state, props) {
    let { history } = props;
    let oldQuery = history.location.search;
    let newQuery = "";
    if (state.doctorName) {
      newQuery += `doc=${state.doctorName}&`;
    }
    if (state.doctorType) {
      newQuery += `docType=${state.doctorType}&`;
    }
    if (state.date) {
      newQuery += `date=${state.date}&`;
    }
    newQuery = newQuery.substr(0, newQuery.length - 1);
    if (newQuery != oldQuery) {
      history.push(history.location.pathname + '?' + newQuery);
    }
    props.loadDoctorTalons(state);
  }

  onFilter(e, state) {
    this.loadTalonsByState(state, this.props);
  }

  componentWillUpdate(nextProps, nextState){
    let {approvedTalon, history} = nextProps;
    if ( approvedTalon && !approvedTalon.proceed){
      approvedTalon.proceed = true;
      nextProps.loadDoctorTalons(nextState, true);
      history.push('/my-talons');
      // location.pathname = "/my-talons";
    }
  }

  render() {
    let { doctorsTalons = [], requestInProgress, approveRequestInProgress, approvedTalon } = this.props;
    let { modalOpen, talon } = this.state;
    return (<BaseScene>
      <CommonPageGrid activeIndex={1}>
        <CommonPageRightPanel title="Талоны к врачу">
          <FilterComponent possibleDoctorTypes={possibleDoctorTypes} possibleAreas={possibleAreas}
            btnName="Найти талоны" onFormSubmit={this.onFilter} onLoadCallBack={state => this.loadTalonsByState(state, this.props)} />
          <DoctorTimetable doctors={doctorsTalons} headers={['Врач', 'Время записи', 'Действие']} renderRow={renderRow}
            onSubmit={this.onTalonClickSubmit} />
          {requestInProgress && <LocalLoadingComponent />}
          {modalOpen &&
            <SubmitTalonModal onModalClose={this.onModalClose} onReject={this.onModalClose} onApprove={this.onTalonApprove}>
              <div className={styles.talon_modal_body}>
                Вы действительно хотите подтвердить запись к доктору <strong>{talon.docName}</strong>&nbsp;
                (специальность <strong>{talon.docSpec}</strong>) <br/> на <strong>{talon.talonDate}</strong>?
              </div>
            </SubmitTalonModal>}
            {approveRequestInProgress && <LoadingComponent/>}
        </CommonPageRightPanel>
      </CommonPageGrid>
      {/* {requestInProgress && <LoadingComponent />} */}
    </BaseScene>
    )
  }
}


class SubmitTalonModal extends Component {
  render() {
    let { onModalClose, onReject, onApprove } = this.props;
    return (<Modal isOpen={true} onModalClose={onModalClose}>
      <ModalHeader title="Подвердить запись к доктору" onModalClose={onModalClose} />
      <ModalContent>{this.props.children}</ModalContent>
      <ModalFooter>
        <div className={styles.talon_modal_footer}>
          <button onClick={onReject} className={`${commonStyles.big} ${commonStyles.inactive}`}>Отмена</button>
          <button onClick={onApprove} className={commonStyles.big}>Заказать</button>
        </div>
      </ModalFooter>
    </Modal>
    )
  }
}

const renderRow = (doc) => {
  return [
    <div key="name">
      <p className={commonStyles.bold_par}>
        {doc.docName}
      </p>
      {doc.speciality}
      <input type="hidden" value={doc.docName} name="docName" />
      <input type="hidden" value={doc.speciality} name="docSpec" />
    </div>,
    <div key="talons-select">
      <select name="talonDate" id={`talon-${doc.doc_id}`}>
        {doc.talons.map((talon, i) => (
          <option value={talon.id} key={i}>{talon.timeStr}</option>
        ))}
      </select>
    </div>,
    <div key="submit">
      {window.userLogged && <button type="submit">Запись</button>}
      {!window.userLogged && <Link to="/login">Войдите, чтобы записаться</Link>}
    </div>
  ];
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadDoctorTalons: (filters, force = false) => dispatch(loadDoctorsTalonsAction(filters, force)),
    submitTalon: id => dispatch(submitTalonAction(id))
  }
}

export const Talons = connectCustom('talons', mapDispatchToProps)(TalonsClear);