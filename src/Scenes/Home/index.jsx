import React, { Component } from 'react'
import { BaseScene } from '../BaseScene/index';
import { Panel } from '../../Components/Panel/index';
import { GridLayout, GridItem } from '../../Components/GridLayout/index';
import { PanelWithText } from '../../Components/PanelWithText/index';

import policlinicaImage from './img/policlinica.jpg';
import timeTableImage from './img/time-table.jpg';
import doctorAtHomeImage from './img/doctor-at-home.jpg';
import talonsImage from './img/talons.jpg';
import childImage from './img/child.jpg';
import styles from './scss/index.scss';

export class Home extends Component {
    render() {
        return (
            <BaseScene>
                <PanelWithText title="Информация о поликлинике №189" img={policlinicaImage}
                    text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget." />
                <GridLayout>
                    <Panel title="Расписание врачей" img={timeTableImage} href="/timetable" />
                    <Panel title="Вызвать врача" img={doctorAtHomeImage} href="/talons" />
                    <Panel title="Талоны" img={talonsImage} href="/talons" />
                    <div className={styles.txt_panel}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
                    </div>
                    {/* <Panel title="Записать ребенка" img={childImage} href="/talons" /> */}
                </GridLayout>
            </BaseScene>
        )
    }
}
