import React, { Component } from 'react'
import { connectCustom } from '../../common/reduxTools';
import { BaseScene } from '../BaseScene/index';
import styles from './scss/index.scss';
import commonStyles from '../../scss/common.scss';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment';
import { createUserAction } from '../../common/userReducer';
import { LoadingComponent } from '../../Components/LoadingComponent/index';

class RegisterPage extends Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.isFormValid = this.isFormValid.bind(this);
    this.performRegister = this.performRegister.bind(this);
    this.state = {
      login: '',
      password: '',
      repeatPassword: '',
      birthday: '',
      userName: '',
      errors: {},
      startEditRepeat: false
    }
  }

  stateAfterValidate = (state, name) => {
    let errors = { ...state.errors };
    const value = state[name];
    errors[name] = !value;
    if (name == 'repeatPassword') {
      state.startEditRepeat = true;
    }
    if (state.startEditRepeat) {
      errors.repeatPassword = (state.repeatPassword !== state.password);
    }
    return { errors };
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    }, () => {
      this.setState(this.stateAfterValidate(this.state, name))
    });
  }

  performRegister(e) {
    e.preventDefault();
    let { login, password, birthday, userName } = this.state;
    this.props.registerUser(login, userName, password, birthday);
  }

  isFormValid() {
    let { login, password, errors = {}, repeatPassword, startEditRepeat, userName } = this.state;
    return login && password && (password == repeatPassword) && userName;
  }

  render() {
    let { login, password, errors = {}, repeatPassword, startEditRepeat, userName } = this.state;
    let { login: invalidLogin, password: invalidPassword, repeatPassword: repeatPasswordError, userName: invalidUserName } = errors;
    let { loginInProgress, userAlreadyExists } = this.props;
    return (
      <BaseScene>
        <div className={styles.register_scene}>
          <h1 className={commonStyles.text_center}>Регистрация</h1>
          {userAlreadyExists && <div className={commonStyles.danger_global_msg}>
            <i className="fa fa-exclamation-triangle" />&nbsp;Пользовель с таким именем уже существует</div>}
          <form onSubmit={this.performRegister}>
            <div className={styles.input_row}>
              <label htmlFor="login"><i className="fa fa-user" />&nbsp;Введите имя пользователя:*</label>
              <input type="text" id="login"
                placeholder="Имя пользователя" value={login}
                onChange={this.handleInputChange} name="login"
                className={invalidLogin ? styles.invalid : ''} />
              {invalidLogin && <div className={styles.error_msg}>Введите имя пользователя</div>}
            </div>
            <div className={styles.input_row}>
              <label htmlFor="userName"><i className="fa fa-user" />&nbsp;Введите Ваше имя и фамилию:*</label>
              <input type="text" id="userName"
                placeholder="Имя и Фамилия" value={userName}
                onChange={this.handleInputChange} name="userName"
                className={invalidUserName ? styles.invalid : ''} />
              {invalidUserName && <div className={styles.error_msg}>Введите имя и фамилию</div>}
            </div>
            <div className={styles.input_row}>
              <label htmlFor="password"><i className="fa fa-key" />&nbsp;Введите пароль:*</label>
              <input type="password" id="password" placeholder="Пароль" value={password}
                onChange={this.handleInputChange} name="password"
                className={invalidPassword ? styles.invalid : ''} />
              {invalidPassword && <div className={styles.error_msg}>Введите пароль</div>}
            </div>
            {/* repeatPassword  section start*/}
            <div className={styles.input_row}>
              <label htmlFor="repeatPassword"><i className="fa fa-key" />&nbsp;Подвердите пароль:*</label>
              <input type="password" id="repeatPassword" placeholder="Повторный пароль" value={repeatPassword}
                onChange={this.handleInputChange} name="repeatPassword"
                className={repeatPasswordError ? styles.invalid : ''} />
              {repeatPasswordError && <div className={styles.error_msg}>Пароли не совпадают</div>}
            </div>
            {/* repeatPassword section end */}
            {/* birthday start*/}
            <div className={styles.input_row}>
              <label htmlFor="repeatPassword"><i className="fa fa-calendar" />&nbsp;Дата рождения:*</label>
              <DayPickerInput onDayChange={(selectedDate) => {
                console.log(selectedDate);
                this.setState({
                  birthday: formatDate(selectedDate, "DD-MM-YYYY")
                })
              }}
                formatDate={formatDate}
                parseDate={parseDate}
                format="DD-MM-YYYY"
              />
            </div>
            {/* birthday end */}
            <div className={`${commonStyles.text_right} ${styles.submit_section}`}>
              <button className={commonStyles.big}
                disabled={!this.isFormValid()}>
                Зарегистрироваться
              </button>
            </div>
          </form>
        </div>
        {loginInProgress && <LoadingComponent />}
      </BaseScene>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    registerUser: (login, userName, password, birthday) => dispatch(createUserAction(login, userName, password, birthday))
  }
}

export const Register = connectCustom('user', mapDispatchToProps)(RegisterPage);