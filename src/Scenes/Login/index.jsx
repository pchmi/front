import React, { Component } from 'react'
import { BaseScene } from '../BaseScene/index';
import { connectCustom } from '../../common/reduxTools';
import styles from './scss/index.scss';
import commonStyles from '../../scss/common.scss';
import { loginUserAction } from '../../common/userReducer';
import { LoadingComponent } from '../../Components/LoadingComponent/index';

class LoginScene extends Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.performLogin = this.performLogin.bind(this);
    this.checkUserLogged = this.checkUserLogged.bind(this);
    this.callDone = true;
    this.state = {
      login: '',
      password: '',
      errors: {}
    }
  }

  stateAfterValidate = (state, name) => {
    let errors = { ...state.errors };
    const value = state[name];
    if (value) {
      errors[name] = false;
    } else {
      errors[name] = true;
    }
    return { errors };
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    }, () => {
      this.setState(this.stateAfterValidate(this.state, name))
    });
  }

  performLogin(event) {
    event.preventDefault();
    this.callDone = false;
    let { login, password } = this.state;
    this.props.loginUser(login, password);
  }

  checkUserLogged = (data = {}, history) => {
    if (data.Error == 404 && this.state.errorCode != 404) {
      this.setState({ globalErrors: "Нет такого пользователя", errorCode: 404 })
      this.callDone = true;
      return;
    }
    if (data.Error == 400 && this.state.errorCode != 400) {
      this.setState({ globalErrors: "Неверный пароль", errorCode: 400 })
      this.callDone = true;
      return;
    }
    if (data.email && data.id) {
      this.callDone = true;
      history.push('/');
    }
  }

  componentWillUpdate(nextProps, nextState) {
    let { data = {}, history } = nextProps
    // if (!this.callDone) {
      this.checkUserLogged(data, history);
    // }
  }

  render() {
    let { login, password, errors = {}, globalErrors } = this.state;
    let { login: invalidLogin, password: invalidPassword } = errors;
    let { loginInProgress } = this.props;
    return (<BaseScene>
      <div className={styles.login_scene}>
        <h1 className={commonStyles.text_center}>Вход в систему</h1>
        {globalErrors && <div className={commonStyles.danger_global_msg}><i className="fa fa-exclamation-triangle" />&nbsp;{globalErrors}</div>}
        <form onSubmit={this.performLogin}>
          <div className={styles.input_row}>
            <label htmlFor="login"><i className="fa fa-user" /></label>
            <input type="text" id="login"
              placeholder="Имя пользователя" value={login}
              onChange={this.handleInputChange} name="login"
              className={invalidLogin ? styles.invalid : ''} />
            {invalidLogin && <div className={styles.error_msg}>Введите имя пользователя</div>}
          </div>
          <div className={styles.input_row}>
            <label htmlFor="password"><i className="fa fa-key" /></label>
            <input type="password" id="password" placeholder="Пароль" value={password}
              onChange={this.handleInputChange} name="password"
              className={invalidPassword ? styles.invalid : ''} />
            {invalidPassword && <div className={styles.error_msg}>Введите пароль</div>}
          </div>
          <div className={`${commonStyles.text_right} ${styles.submit_section}`}>
            <button className={commonStyles.big} disabled={invalidLogin || invalidPassword}>Войти в систему</button>
          </div>
        </form>
      </div>
      {loginInProgress && <LoadingComponent />}
    </BaseScene>)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loginUser: (userName, password) => dispatch(loginUserAction(userName, password))
  };
}
export const Login = connectCustom('user', mapDispatchToProps)(LoginScene);