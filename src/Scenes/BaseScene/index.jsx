import React, { Component } from 'react'
import { Header } from '../../Components/Header/index';
import { Footer } from '../../Components/Footer/index';
import styles from './scss/index.scss';

export class BaseScene extends Component {
    render() {
        return <div className={styles.main}>
            <Header title="Талоны"/>
            {this.props.children}
            <Footer footerLeftTitle="Мы в соцсетях:" title="Талоны"/>
        </div>
    }
}
