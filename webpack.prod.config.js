const merge = require('webpack-merge');
const baseConfig = require('./webpack.base.config');
var webpack = require('webpack');

var conf = merge(baseConfig, {
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: true
            }
        })
    ],
  });

module.exports = conf;
